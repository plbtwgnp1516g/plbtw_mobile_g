package com.pam.plbtw_g;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

public class ActivityDeskripsi extends AppCompatActivity {
    private TextView nama,tag,deskripsi;
    private ScrollView scrollView;
    private String nama_konten,deskripsi_konten,tag_konten;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deskripsi);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        deskripsi=(TextView)findViewById(R.id.txtDeskripsi);
        tag=(TextView)findViewById(R.id.txttag);
        nama=(TextView)findViewById(R.id.txtnama);

        Bundle b = getIntent().getExtras();
        tag_konten = b.getString("tag_konten");
        nama_konten = b.getString("nama_konten");
        deskripsi_konten = b.getString("deskripsi_konten");

        deskripsi.setText(deskripsi_konten);
        tag.setText(tag_konten);
        getSupportActionBar().setTitle(nama_konten);
        nama.setVisibility(View.GONE);//setText(nama_konten);
    }
}
