package com.pam.plbtw_g;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class ActivityDeskripsi extends AppCompatActivity {
    private TextView nama,tag,deskripsi;
    private ScrollView scrollView;
    private String nama_konten,deskripsi_konten,tag_konten, gambar_konten;
    CollapsingToolbarLayout toolbar_layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deskripsi);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        toolbar_layout=(CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        deskripsi=(TextView)findViewById(R.id.txtDeskripsi);
        tag=(TextView)findViewById(R.id.txttag);
        nama=(TextView)findViewById(R.id.txtnama);

        Bundle b = getIntent().getExtras();
        tag_konten = b.getString("tag_konten");
        nama_konten = b.getString("nama_konten");
        deskripsi_konten = b.getString("deskripsi_konten");
        gambar_konten = b.getString("gambar_konten");
        deskripsi.setText(deskripsi_konten);
        tag.setText(tag_konten);
        getSupportActionBar().setTitle(nama_konten);
        nama.setVisibility(View.GONE);//setText(nama_konten);
        String url="http://plbtwtraditionalculture.16mb.com/traditional_culture_api/uploads/"+gambar_konten;

        Picasso.with(this).load(url).into(new Target(){

            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                toolbar_layout.setBackground(new BitmapDrawable(getApplicationContext().getResources(), bitmap));
            }

            @Override
            public void onBitmapFailed(final Drawable errorDrawable) {
            }

            @Override
            public void onPrepareLoad(final Drawable placeHolderDrawable) {
            }
        });
    }
}
