package com.pam.plbtw_g.Adapter;

/**
 * Created by PIER on 5/25/2016.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.api.defaults.DefaultBootstrapBrand;
import com.beardedhen.androidbootstrap.font.FontAwesome;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.Types.BoomType;
import com.nightonke.boommenu.Types.ButtonType;
import com.nightonke.boommenu.Types.DimType;
import com.nightonke.boommenu.Types.PlaceType;
import com.nightonke.boommenu.Util;
import com.pam.plbtw_g.ActivityDeskripsi;
import com.pam.plbtw_g.Model.Konten;
import com.pam.plbtw_g.R;
import com.pam.plbtw_g.deskripsi;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class ListKontenAdapter extends RecyclerView.Adapter<ListKontenAdapter.ViewHolder> {

    List<Konten> listKonten;
    private ListKontenAdapterCallback callback;
    private Context context;
    public View iView;
    private int lastPosition = -1;

    private Konten modelKonten;

    public ListKontenAdapter(Context context, ArrayList<Konten> konten, String Tag) {
        super();

        this.context = context;

        listKonten = new ArrayList<Konten>();


        for (int i = 0; i < konten.size(); i++) {
            if(Tag.equals(konten.get(i).getTag_konten()))
            {
                modelKonten = new Konten();
                modelKonten.setFlag_rate(konten.get(i).getFlag_rate());
                modelKonten.setId_konten(konten.get(i).getId_konten());
                modelKonten.setId_user(konten.get(i).getId_user());
                modelKonten.setDeskripsi_konten(konten.get(i).getDeskripsi_konten());
                modelKonten.setGambar_konten(konten.get(i).getGambar_konten());
                modelKonten.setLokasi_konten(konten.get(i).getLokasi_konten());
                modelKonten.setNama_konten(konten.get(i).getNama_konten());
                modelKonten.setTag_konten(konten.get(i).getTag_konten());
                modelKonten.setTotal_rate(konten.get(i).getTotal_rate());
                listKonten.add(modelKonten);
            }

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_konten, viewGroup, false);

        View d = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_konten, null);
        ViewHolder viewHolder = new ViewHolder(d);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int j) {
        Konten modelKonten = listKonten.get(j);
        int rate=Integer.parseInt(modelKonten.getTotal_rate());
        final String idkonten=modelKonten.getId_konten();
        final String flag = modelKonten.getFlag_rate();
        viewHolder.txttotalrate.setText(modelKonten.getTotal_rate()+" Points");
        viewHolder.txtnama.setText(modelKonten.getNama_konten());
        viewHolder.txtlokasi.setText(modelKonten.getLokasi_konten());
     //   Picasso.with(context).load("http://semartcompany.16mb.com/assets/img/produk/" + modelProduk.getFoto_produk())
     //           .error(R.drawable.logo)
     //           .placeholder(R.drawable.icon)
     //           .into(viewHolder.imgThumbnail);
        String url="http://plbtwtraditionalculture.16mb.com/traditional_culture_api/uploads/"+modelKonten.getGambar_konten();
        Picasso.with(context).load(url).into(viewHolder.imgThumbnail);

        viewHolder.but.setRounded(true);
        viewHolder.but.setShowOutline(false);
        viewHolder.but.setFontAwesomeIcon(FontAwesome.FA_THUMBS_UP);
        viewHolder.but.setBootstrapSize(1.5f);

        viewHolder.but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(callback != null) {
                    if(viewHolder.but.getBootstrapBrand()==DefaultBootstrapBrand.REGULAR
                            && viewHolder.but2.getBootstrapBrand()==DefaultBootstrapBrand.REGULAR)
                    {
                        viewHolder.but2.setBootstrapBrand(DefaultBootstrapBrand.REGULAR);
                        viewHolder.but.setBootstrapBrand(DefaultBootstrapBrand.SUCCESS);
                        callback.upvote(idkonten);
                    }
                    else if(viewHolder.but.getBootstrapBrand()==DefaultBootstrapBrand.REGULAR
                            && viewHolder.but2.getBootstrapBrand()==DefaultBootstrapBrand.DANGER)
                    {
                        viewHolder.but2.setBootstrapBrand(DefaultBootstrapBrand.REGULAR);
                        viewHolder.but.setBootstrapBrand(DefaultBootstrapBrand.SUCCESS);
                        callback.upvote(idkonten);
                        callback.upvote(idkonten);
                    }
                    else
                    {
                        viewHolder.but.setBootstrapBrand(DefaultBootstrapBrand.REGULAR);
                        callback.normalvote(idkonten);
                    }

                }
            }
        });
        viewHolder.but2.setRounded(true);
        viewHolder.but2.setShowOutline(false);
        viewHolder.but2.setFontAwesomeIcon(FontAwesome.FA_THUMBS_DOWN);
        viewHolder.but2.setBootstrapSize(1.5f);
        if(flag.equals("1"))
        viewHolder.but2.setBootstrapBrand(DefaultBootstrapBrand.DANGER);
        else if(flag.equals("2"))
            viewHolder.but.setBootstrapBrand(DefaultBootstrapBrand.SUCCESS);
        else {
            viewHolder.but2.setBootstrapBrand(DefaultBootstrapBrand.REGULAR);
            viewHolder.but.setBootstrapBrand(DefaultBootstrapBrand.REGULAR);
        }



        viewHolder.but2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(callback != null) {
                    if(viewHolder.but2.getBootstrapBrand()==DefaultBootstrapBrand.REGULAR
                            && viewHolder.but.getBootstrapBrand()==DefaultBootstrapBrand.REGULAR)
                    {
                        viewHolder.but.setBootstrapBrand(DefaultBootstrapBrand.REGULAR);
                        viewHolder.but2.setBootstrapBrand(DefaultBootstrapBrand.DANGER);
                        callback.downvote(idkonten);
                    }
                    else if(viewHolder.but2.getBootstrapBrand()==DefaultBootstrapBrand.REGULAR
                            && viewHolder.but.getBootstrapBrand()==DefaultBootstrapBrand.SUCCESS)
                    {
                        viewHolder.but.setBootstrapBrand(DefaultBootstrapBrand.REGULAR);
                        viewHolder.but2.setBootstrapBrand(DefaultBootstrapBrand.DANGER);
                        callback.downvote(idkonten);
                        callback.downvote(idkonten);
                    }
                    else
                    {
                        viewHolder.but2.setBootstrapBrand(DefaultBootstrapBrand.REGULAR);
                        callback.normalvote(idkonten);
                    }
                }
            }
        });

        viewHolder.currentItem = listKonten.get(j);
    }

    private String[] Colors = {
            "#F44336",
            "#E91E63",
            "#9C27B0",
            "#2196F3",
            "#03A9F4",
            "#00BCD4",
            "#009688",
            "#4CAF50",
            "#8BC34A",
            "#CDDC39",
            "#FFEB3B",
            "#FFC107",
            "#FF9800",
            "#FF5722",
            "#795548",
            "#9E9E9E",
            "#607D8B"};

    public int GetRandomColor() {
        Random random = new Random();
        int p = random.nextInt(Colors.length);
        return Color.parseColor(Colors[p]);
    }
    public void setCallback(ListKontenAdapterCallback callback){

        this.callback = callback;
    }


    public interface ListKontenAdapterCallback {

        public void upvote(String idkonten);
        public void downvote(String idkonten);
        public void normalvote(String idkonten);
    }
    @Override
    public int getItemCount() {
        return listKonten.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView imgThumbnail;
        public TextView txtnama, txtlokasi,txttotalrate;
        public BoomMenuButton circleBoomMenuButton;
        public Konten currentItem;
        public View cardcontent;
        public BootstrapButton but,but2;
        public ViewHolder(View itemView) {
            super(itemView);
            iView=itemView;
            imgThumbnail = (ImageView)itemView.findViewById(R.id.imageViewFoto);
            txtnama = (TextView)itemView.findViewById(R.id.namakonten);
            txtlokasi = (TextView)itemView.findViewById(R.id.lokasi);
            cardcontent =itemView.findViewById(R.id.card_content);
            but=(BootstrapButton) itemView.findViewById(R.id.but);
            but2=(BootstrapButton) itemView.findViewById(R.id.but2);
            txttotalrate=(TextView)itemView.findViewById(R.id.pointlokasi);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i = new Intent(view.getContext(), ActivityDeskripsi.class);


                    Bundle b = new Bundle();
                    b.putString("id_user", String.valueOf(currentItem.getId_user()));
                    b.putString("deskripsi_konten", String.valueOf(currentItem.getDeskripsi_konten()));
                    b.putString("gambar_konten", String.valueOf(currentItem.getGambar_konten()));
                    b.putString("lokasi_konten", String.valueOf(currentItem.getLokasi_konten()));
                    b.putString("nama_konten", String.valueOf(currentItem.getNama_konten()));
                    b.putString("tag_konten", String.valueOf(currentItem.getTag_konten()));
                    b.putString("total_rate", String.valueOf(currentItem.getTotal_rate()));

                    i.putExtras(b);
                  //  Pair<View, String> pair1= Pair.create(view, "imagetoshow");
                   // ActivityOptionsCompat options = ActivityOptionsCompat.
                   //         makeSceneTransitionAnimation((Activity)context,pair1);

                  //  ActivityOptionsCompat options = ActivityOptionsCompat.
                    //        makeSceneTransitionAnimation((Activity)context, (View)imgThumbnail, "imagetoshow");

                    context.startActivity(i);

                }
            });
        }
    }
}
