package com.pam.plbtw_g.Fragment;


import android.animation.Animator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ShareCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.akexorcist.roundcornerprogressbar.IconRoundCornerProgressBar;
import com.dd.CircularProgressButton;
import com.google.gson.Gson;
import com.jpardogo.android.googleprogressbar.library.ChromeFloatingCirclesDrawable;
import com.jpardogo.android.googleprogressbar.library.FoldingCirclesDrawable;
import com.jpardogo.android.googleprogressbar.library.NexusRotationCrossDrawable;
import com.pam.plbtw_g.Model.Konten;
import com.pam.plbtw_g.Model.Rate;
import com.pam.plbtw_g.Model.ResultAPI;
import com.pam.plbtw_g.Model.TotalRate;
import com.pam.plbtw_g.Model.User;
import com.pam.plbtw_g.R;
import com.pam.plbtw_g.Rest.RestClient;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChallengeFragment extends Fragment {
    ProgressBar progressBar;
    IconRoundCornerProgressBar progress_1,progress_2,progress_3,progress_4,progress_5;
    TextView txtpoint1,txtpoint2,txtpoint3,txtpoint4,txtpoint5;
    CircularProgressButton btnredeem1,btnredeem2,btnredeem3,btnredeem4,btnredeem5;
    LinearLayout challengecontainer;
    String iduser;
    Context context;
    String TotalRate;
    ArrayList<User> UserData = new ArrayList<User>();
    ArrayList<CircularProgressButton> btnredeemlist = new ArrayList<CircularProgressButton>();
    RestClient.GitApiInterface service;
    Call<ResultAPI<TotalRate>> callRate;
    Call<ResultAPI<Rate>> callRedeem;
    int TRate;
    Call<ResultAPI<User>> callUser;
    public ChallengeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_challenge, container, false);
        context=getActivity().getApplicationContext();
        SharedPreferences sp=context.getSharedPreferences("MyShared", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor=sp.edit();

        iduser = sp.getString("id_user","");
        Log.d("GG",iduser);
        final View myView = rootView.findViewById(R.id.container);
        TRate=0;
        progressBar=(ProgressBar) rootView.findViewById(R.id.progressbar);
        progress_1=(IconRoundCornerProgressBar) rootView.findViewById(R.id.progress_1);
        progress_2=(IconRoundCornerProgressBar) rootView.findViewById(R.id.progress_2);
        progress_3=(IconRoundCornerProgressBar) rootView.findViewById(R.id.progress_3);
        progress_4=(IconRoundCornerProgressBar) rootView.findViewById(R.id.progress_4);
        progress_5=(IconRoundCornerProgressBar) rootView.findViewById(R.id.progress_5);
        txtpoint1=(TextView) rootView.findViewById(R.id.txtpoint1);
        txtpoint2=(TextView) rootView.findViewById(R.id.txtpoint2);
        txtpoint3=(TextView) rootView.findViewById(R.id.txtpoint3);
        txtpoint4=(TextView) rootView.findViewById(R.id.txtpoint4);
        txtpoint5=(TextView) rootView.findViewById(R.id.txtpoint5);
        btnredeem1=(CircularProgressButton) rootView.findViewById(R.id.btnredeem1);
        btnredeem2=(CircularProgressButton) rootView.findViewById(R.id.btnredeem2);
        btnredeem3=(CircularProgressButton) rootView.findViewById(R.id.btnredeem3);
        btnredeem4=(CircularProgressButton) rootView.findViewById(R.id.btnredeem4);
        btnredeem5=(CircularProgressButton) rootView.findViewById(R.id.btnredeem5);
        challengecontainer=(LinearLayout)rootView.findViewById(R.id.challengecontainer);
        btnredeemlist.add(btnredeem1);
        btnredeemlist.add(btnredeem2);
        btnredeemlist.add(btnredeem3);
        btnredeemlist.add(btnredeem4);
        btnredeemlist.add(btnredeem5);

        progressBar.setIndeterminateDrawable(new ChromeFloatingCirclesDrawable.Builder(context)
                .build());

        btnredeem1.setEnabled(false);
        btnredeem2.setEnabled(false);
        btnredeem3.setEnabled(false);
        btnredeem4.setEnabled(false);
        btnredeem5.setEnabled(false);


        getTotalRate();

        //if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        //progressBar.setProgressTintList(ColorStateList.valueOf(Color.WHITE));
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            rootView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    v.removeOnLayoutChangeListener(this);
                    int cx = (myView.getLeft() + myView.getRight()) / 2;
                    int cy = (myView.getTop() + myView.getBottom()) / 2;

                    // get the final radius for the clipping circle
                    int dx = Math.max(cx, myView.getWidth() - cx);
                    int dy = Math.max(cy, myView.getHeight() - cy);
                    float finalRadius = (float) Math.hypot(dx, dy);

                    // Android native animator
                    Animator animator =
                            ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);
                    animator.setInterpolator(new AccelerateDecelerateInterpolator());
                    animator.setDuration(500);
                    animator.start();
                }
            });
        }
        return rootView;
    }
    public void getUserData()
    {
        service = RestClient.getClient();
        callUser = service.getUserData(iduser);

        callUser.enqueue(new Callback<ResultAPI<User>>() {
            @Override
            public void onResponse(Response<ResultAPI<User>> response) {
                //progressimage.setVisibility(View.GONE);
                //notfound.setVisibility(View.GONE);
                Log.d("MainActivity", "Status Code = " + response.code());
                if (response.isSuccess()) {
                    // request successful (status code 200, 201)
                    ResultAPI<User> result = response.body();
                    UserData=result.getUserData();
                    onGetUserData(UserData);
                    Log.d("BACA", "response = " + new Gson().toJson(result));
                    progressBar.setVisibility(View.GONE);
                    challengecontainer.setVisibility(View.VISIBLE);
                    //onGetTotalRates(Integer.parseInt(TotalRate));

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors
                    Log.d("MainActivity", "error");
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("on Failure", t.toString());
                //Progress.setVisibility(View.GONE);
                //progressimage.setVisibility(View.GONE);
                //notfound.setVisibility(View.GONE);
                Log.d("MainActivity", "gagalall");
                //Toast.makeText(v.getContext(), "Cek Koneksi Anda", Toast.LENGTH_LONG).show();
            }
        });
    }
    public void onGetUserData(ArrayList<User> UserData)
    {

        if(!(TotalRate.equals("No Data Found")))
        TRate=Integer.parseInt(TotalRate);

        if(TRate!=0)
        {
            start(100,progress_1,TRate,txtpoint1,btnredeem1);
            start(200,progress_2,TRate,txtpoint2,btnredeem2);
            start(300,progress_3,TRate,txtpoint3,btnredeem3);
            start(400,progress_4,TRate,txtpoint4,btnredeem4);
            start(500,progress_5,TRate,txtpoint5,btnredeem5);
        }

    }
    public void getTotalRate()
    {
        service = RestClient.getClient();
        callRate = service.getTotalRate(iduser);

        callRate.enqueue(new Callback<ResultAPI<TotalRate>>() {
            @Override
            public void onResponse(Response<ResultAPI<TotalRate>> response) {
                //progressimage.setVisibility(View.GONE);
                //notfound.setVisibility(View.GONE);
                Log.d("MainActivity", "Status Code = " + response.code());
                if (response.isSuccess()) {
                    // request successful (status code 200, 201)
                    ResultAPI<TotalRate> result = response.body();
                    TotalRate = result.getTotalRates();
                    Log.d("BACA", "response = " + TotalRate);
                    //onGetTotalRates(Integer.parseInt(TotalRate));
                    getUserData();

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors
                    Log.d("MainActivity", "error");
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("on Failure", t.toString());
                //Progress.setVisibility(View.GONE);
                //progressimage.setVisibility(View.GONE);
                //notfound.setVisibility(View.GONE);
                Log.d("MainActivity", "gagalall");
                //Toast.makeText(v.getContext(), "Cek Koneksi Anda", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void start(final int max, final IconRoundCornerProgressBar progressbar, final int progress, final TextView txtpoint, final CircularProgressButton btnredeem)
    {
        final Timer timer;
        timer = new Timer();
        progressbar.setMax(max);
        progressbar.setProgress(0);
        txtpoint1.setText("0 Of "+max+" Votes" );

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    (getActivity()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressbar.setProgress(progressbar.getProgress()+1);
                            txtpoint.setText((int)progressbar.getProgress()+" Of "+max+" Votes" );

                            if(progressbar.getProgress()>=max || progressbar.getProgress()>=progress ) {
                                timer.cancel();
                                //progressbar.setProgress(max);
                                //txtpoint1.setText((int)progress+" Of "+max+" Votes" );
                                if(progressbar.getProgress()==max)
                                {
                                    enablebutton(btnredeem);
                                }
                            }
                        }
                    });}catch (NullPointerException eee)
                {

                }
            }
        }, 0, 10);


    }
    public String IsRedeem(int index)
    {
        String isredeem="";
        switch (index)
        {
            case 0:
                isredeem=UserData.get(0).getChallenge_1();
                break;
            case 1:
                isredeem=UserData.get(0).getChallenge_2();
                break;
            case 2:
                isredeem=UserData.get(0).getChallenge_3();
                break;
            case 3:
                isredeem=UserData.get(0).getChallenge_4();
                break;
            case 4:
                isredeem=UserData.get(0).getChallenge_5();
                break;
        }
        return isredeem;
    }
    public void SetRedeem(int index)
    {
        switch (index)
        {
            case 0:
                UserData.get(0).setChallenge_1("1");
                break;
            case 1:
                UserData.get(0).setChallenge_2("1");
                break;
            case 2:
                UserData.get(0).setChallenge_3("1");
                break;
            case 3:
                UserData.get(0).setChallenge_4("1");
                break;
            case 4:
                UserData.get(0).setChallenge_5("1");
                break;
        }
    }
    public void enablebutton(final CircularProgressButton btnredeem)
    {
        for(int i=0; i<=5 ; i++)
        {
            if(IsRedeem(i).equals("1") && btnredeem==btnredeemlist.get(i))
            {
                btnredeem.setProgress(100);
                btnredeem.setEnabled(true);
            }
            else if(IsRedeem(i).equals("0") && btnredeem==btnredeemlist.get(i))
            {
                final int temp=i;
                btnredeem.setEnabled(true);
                btnredeem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        btnredeem.setIndeterminateProgressMode(true);
                        btnredeem.setProgress(50);
                        redeemchallenge(temp);

                    }
                });
            }
        }

    }
    public void redeemchallenge(final int challengeno)
    {
        if(IsRedeem(challengeno).equals("0")) {
            service = RestClient.getClient();
            callRedeem = service.redeemChallenge(iduser, challengeno + 1, 1000);

            callRedeem.enqueue(new Callback<ResultAPI<Rate>>() {
                @Override
                public void onResponse(Response<ResultAPI<Rate>> response) {
                    //progressimage.setVisibility(View.GONE);
                    //notfound.setVisibility(View.GONE);
                    Log.d("MainActivity", "Status Code = " + response.code());
                    if (response.isSuccess()) {
                        // request successful (status code 200, 201)
                        btnredeemlist.get(challengeno).setProgress(100);
                        SetRedeem(challengeno);
                    } else {
                        // response received but request not successful (like 400,401,403 etc)
                        //Handle errors
                        Log.d("MainActivity", "error");
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    Log.e("on Failure", t.toString());
                    //Progress.setVisibility(View.GONE);
                    //progressimage.setVisibility(View.GONE);
                    //notfound.setVisibility(View.GONE);
                    Log.d("MainActivity", "gagalall");
                    //Toast.makeText(v.getContext(), "Cek Koneksi Anda", Toast.LENGTH_LONG).show();
                }
            });

        }
    }
}
