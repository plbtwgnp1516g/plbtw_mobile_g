package com.pam.plbtw_g.Fragment;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.jpardogo.android.googleprogressbar.library.ChromeFloatingCirclesDrawable;
import com.jpardogo.android.googleprogressbar.library.FoldingCirclesDrawable;
import com.pam.plbtw_g.Adapter.ListKontenAdapter;
import com.pam.plbtw_g.Model.Konten;
import com.pam.plbtw_g.Model.Rate;
import com.pam.plbtw_g.Model.ResultAPI;
import com.pam.plbtw_g.R;
import com.pam.plbtw_g.Rest.RestClient;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentMainan extends Fragment implements ListKontenAdapter.ListKontenAdapterCallback{

    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    ListKontenAdapter mAdapter;

    ProgressBar progressBar;
    ArrayList<Konten> listKonten = new ArrayList<Konten>();

    Call<ResultAPI<Konten>> call;
    Call<ResultAPI<Rate>> callRate;
    RestClient.GitApiInterface service;
    private ListKontenAdapter.ListKontenAdapterCallback callback;
    SwipeRefreshLayout mSwipeRefreshLayout;
    Context context;
    View v;
    String idkonten;
    LinearLayout Progress;
    ImageView progressimage, notfound;
    String iduser;
    public FragmentMainan() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v =  inflater.inflate(R.layout.fragment_lagu, container, false);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        context=getActivity().getApplicationContext();
        callback=this;

        SharedPreferences sp=context.getSharedPreferences("MyShared", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor=sp.edit();

        iduser = sp.getString("id_user","");

        mRecyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        mRecyclerView.setHasFixedSize(true);
        progressBar=(ProgressBar) v.findViewById(R.id.progressbar);
        progressBar.setIndeterminateDrawable(new ChromeFloatingCirclesDrawable.Builder(context)
                .build());
        // The number of Columns
        mLayoutManager = new GridLayoutManager(getActivity(), 1);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipelayout);
        getKatalog();
        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimary,
                R.color.colorPrimary);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Refreshing data on server

                getKatalog();
            }
        });

        return v;
    }


    public void getKatalog()
    {
        service = RestClient.getClient();
        call = service.getKonten(iduser);

        call.enqueue(new Callback<ResultAPI<Konten>>() {
            @Override
            public void onResponse(Response<ResultAPI<Konten>> response) {
                progressBar.setVisibility(View.GONE);
                mSwipeRefreshLayout.setRefreshing(false);
                //progressimage.setVisibility(View.GONE);
                //notfound.setVisibility(View.GONE);
                Log.d("MainActivity", "Status Code = " + response.code());
                if (response.isSuccess()) {
                    // request successful (status code 200, 201)
                    mRecyclerView.setVisibility(View.VISIBLE);
                    ResultAPI<Konten> result = response.body();
                    Log.d("MainActivity", "response = " + new Gson().toJson(result));

                    listKonten = result.getDataKonten();
                    mAdapter = new ListKontenAdapter(v.getContext(), listKonten,"Mainan");
                    mRecyclerView.setAdapter(mAdapter);
                    mAdapter.setCallback(callback);

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors
                    Log.d("MainActivity", "error");
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("on Failure", t.toString());
                //Progress.setVisibility(View.GONE);
                //progressimage.setVisibility(View.GONE);
                //notfound.setVisibility(View.GONE);
                Log.d("MainActivity", "gagalall");
                //Toast.makeText(v.getContext(), "Cek Koneksi Anda", Toast.LENGTH_LONG).show();
            }
        });

    }
    @Override
    public void upvote(String idkonten) {
        service = RestClient.getClient();
        callRate = service.uprate(iduser,idkonten);

        callRate.enqueue(new Callback<ResultAPI<Rate>>() {
            @Override
            public void onResponse(Response<ResultAPI<Rate>> response) {
                if (response.isSuccess()) {
                    // request successful (status code 200, 201)
                } else {
                    // response received but request not successful (like 400,401,403 etc)
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("on Failure", t.toString());
                //Progress.setVisibility(View.GONE);
                //progressimage.setVisibility(View.GONE);
                //notfound.setVisibility(View.GONE);
                Log.d("MainActivity", "gagalall");
                //Toast.makeText(v.getContext(), "Cek Koneksi Anda", Toast.LENGTH_LONG).show();
            }
        });
    }
    @Override
    public void downvote(String idkonten) {
        service = RestClient.getClient();
        callRate = service.downrate(iduser,idkonten);

        callRate.enqueue(new Callback<ResultAPI<Rate>>() {
            @Override
            public void onResponse(Response<ResultAPI<Rate>> response) {
                if (response.isSuccess()) {
                    // request successful (status code 200, 201)
                } else {
                    // response received but request not successful (like 400,401,403 etc)
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("on Failure", t.toString());
                //Progress.setVisibility(View.GONE);
                //progressimage.setVisibility(View.GONE);
                //notfound.setVisibility(View.GONE);
                Log.d("MainActivity", "gagalall");
                //Toast.makeText(v.getContext(), "Cek Koneksi Anda", Toast.LENGTH_LONG).show();
            }
        });
    }
    @Override
    public void normalvote(String idkonten) {
        service = RestClient.getClient();
        callRate = service.normalrate(iduser,idkonten);

        callRate.enqueue(new Callback<ResultAPI<Rate>>() {
            @Override
            public void onResponse(Response<ResultAPI<Rate>> response) {
                if (response.isSuccess()) {
                    // request successful (status code 200, 201)
                } else {
                    // response received but request not successful (like 400,401,403 etc)
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("on Failure", t.toString());
                //Progress.setVisibility(View.GONE);
                //progressimage.setVisibility(View.GONE);
                //notfound.setVisibility(View.GONE);
                Log.d("MainActivity", "gagalall");
                //Toast.makeText(v.getContext(), "Cek Koneksi Anda", Toast.LENGTH_LONG).show();
            }
        });
    }

}
