package com.pam.plbtw_g.Fragment;


import android.animation.Animator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.provider.Contacts;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dd.CircularProgressButton;
import com.google.gson.Gson;
import com.jpardogo.android.googleprogressbar.library.ChromeFloatingCirclesDrawable;
import com.pam.plbtw_g.Model.HistoryData;
import com.pam.plbtw_g.Model.Rate;
import com.pam.plbtw_g.Model.ResultAPI;
import com.pam.plbtw_g.Model.User;
import com.pam.plbtw_g.R;
import com.pam.plbtw_g.Rest.RestClient;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RewardsFragment extends Fragment {

    Context context;
    ProgressBar progressBar;
    ListView history,tanggal;
    LinearLayout challengecontainer;
    private int TotalPoint;
    String iduser;
    View rootView;
    ArrayList<HistoryData> HistoryData = new ArrayList<HistoryData>();
    CircularProgressButton btnredeem1,btnredeem2,btnredeem3,btnredeem4,btnredeem5;
    public RewardsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_rewards, container, false);
        context=getActivity().getApplicationContext();
        SharedPreferences sp=context.getSharedPreferences("MyShared", Activity.MODE_PRIVATE);
        challengecontainer=(LinearLayout)rootView.findViewById(R.id.challengecontainer);
        SharedPreferences.Editor editor=sp.edit();
        history = (ListView)rootView.findViewById(R.id.product);
        tanggal = (ListView)rootView.findViewById(R.id.date);
        iduser = sp.getString("id_user","");
        Log.d("GG",iduser);
        final View myView = rootView.findViewById(R.id.container);
        btnredeem2=(CircularProgressButton) rootView.findViewById(R.id.btnredeem2);
        btnredeem3=(CircularProgressButton) rootView.findViewById(R.id.btnredeem3);
        btnredeem4=(CircularProgressButton) rootView.findViewById(R.id.btnredeem4);
        btnredeem5=(CircularProgressButton) rootView.findViewById(R.id.btnredeem5);


        btnredeem2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TotalPoint>=1000)
                {
                    TotalPoint=TotalPoint-1000;
                    ((TextView)rootView.findViewById(R.id.totalpoints)).setText("Point : "+TotalPoint);
                    RedeemRewards(btnredeem2,1000,"Steam Wallet IDR 12000");
                }

            }
        });
        btnredeem3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TotalPoint>=1500)
                {
                    TotalPoint=TotalPoint-1500;
                    ((TextView)rootView.findViewById(R.id.totalpoints)).setText("Point : "+TotalPoint);
                    RedeemRewards(btnredeem3,1500,"Voucher Diskon Lazada");
                }
            }
        });
        btnredeem4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TotalPoint>=2500)
                {
                    TotalPoint=TotalPoint-2500;
                    ((TextView)rootView.findViewById(R.id.totalpoints)).setText("Point : "+TotalPoint);
                    RedeemRewards(btnredeem4,2500,"Google Cardboard");
                }
            }
        });
        btnredeem5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TotalPoint>=5000)
                {
                    TotalPoint=TotalPoint-5000;
                    ((TextView)rootView.findViewById(R.id.totalpoints)).setText("Point : "+TotalPoint);
                    RedeemRewards(btnredeem5,5000,"Baju Culture Indonesia");
                }
            }
        });
        progressBar=(ProgressBar) rootView.findViewById(R.id.progressbar);
        progressBar.setIndeterminateDrawable(new ChromeFloatingCirclesDrawable.Builder(context)
                .build());
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            rootView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    v.removeOnLayoutChangeListener(this);
                    int cx = (myView.getLeft() + myView.getRight()) / 2;
                    int cy = (myView.getTop() + myView.getBottom()) / 2;

                    // get the final radius for the clipping circle
                    int dx = Math.max(cx, myView.getWidth() - cx);
                    int dy = Math.max(cy, myView.getHeight() - cy);
                    float finalRadius = (float) Math.hypot(dx, dy);

                    // Android native animator
                    Animator animator =
                            ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);
                    animator.setInterpolator(new AccelerateDecelerateInterpolator());
                    animator.setDuration(500);
                    animator.start();
                }
            });
        }
        getRewardHistory();
        return rootView;
    }
    public void RedeemRewards(final CircularProgressButton but,int poin, String productname)
    {
        but.setIndeterminateProgressMode(true);
        but.setProgress(50);

        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(c.getTime());

        RestClient.GitApiInterface service = RestClient.getClient();
        Call<ResultAPI<Rate>> callRedeemHistory = service.redeemRewards(iduser,poin,formattedDate,productname);

        callRedeemHistory.enqueue(new Callback<ResultAPI<Rate>>() {
            @Override
            public void onResponse(Response<ResultAPI<Rate>> response) {
                Log.d("MainActivity", "Status Code = " + response.code());
                if (response.isSuccess()) {
                    but.setProgress(0);
                    getRewardHistory();
                } else {
                    Log.d("MainActivity", "error");
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("on Failure", t.toString());
                Log.d("MainActivity", "gagal");
            }
        });
    }
    public void getRewardHistory()
    {
        RestClient.GitApiInterface service = RestClient.getClient();
        Call<ResultAPI<HistoryData>> callRedeemHistory = service.getUserRedeemHistory(iduser);

        callRedeemHistory.enqueue(new Callback<ResultAPI<HistoryData>>() {
            @Override
            public void onResponse(Response<ResultAPI<HistoryData>> response) {
                Log.d("MainActivity", "Status Code = " + response.code());
                if (response.isSuccess()) {
                    ResultAPI<HistoryData> result = response.body();
                    HistoryData = result.getHistoryData();
                    onGetHistoryData(HistoryData);
                    Log.d("BACA", "response = " + new Gson().toJson(result));
                    getTotalPoints();

                } else {
                    Log.d("MainActivity", "error");
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("on Failure", t.toString());
                Log.d("MainActivity", "gagalall");
                getTotalPoints();
            }
        });
    }

    public void getTotalPoints()
    {
        RestClient.GitApiInterface service = RestClient.getClient();
        Call<ResultAPI> callPoints = service.getTotalPoints(iduser);

        callPoints.enqueue(new Callback<ResultAPI>() {
            @Override
            public void onResponse(Response<ResultAPI> response) {
                Log.d("MainActivity", "Status Code = " + response.code());
                if (response.isSuccess()) {
                    ResultAPI result = response.body();
                    TotalPoint = Integer.parseInt(result.getTotalPoints());
                    Log.d("BACA", "response = " + new Gson().toJson(result));
                    progressBar.setVisibility(View.GONE);
                    challengecontainer.setVisibility(View.VISIBLE);
                    ((TextView)rootView.findViewById(R.id.totalpoints)).setText("Point : "+TotalPoint);

                } else {
                    Log.d("MainActivity", "error");
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("on Failure", t.toString());
                Log.d("MainActivity", "gagalall");
            }
        });
    }
    public void onGetHistoryData(ArrayList<HistoryData> temp)
    {

        ArrayList<String> temp2 = new ArrayList<String>();
        ArrayList<String> temp3 = new ArrayList<String>();
        for(int i=0;i<temp.size();i++)
        {
            temp2.add(temp.get(i).getProduct_name());
            temp3.add(temp.get(i).getRedeem_date());
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                context,
                android.R.layout.simple_list_item_1,
                temp2 );

        history.setAdapter(arrayAdapter);

        ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<String>(
                context,
                android.R.layout.simple_list_item_1,
                temp3 );

        tanggal.setAdapter(arrayAdapter2);


    }
}
