package com.pam.plbtw_g;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.TypefaceProvider;
import com.dd.CircularProgressButton;
import com.google.gson.Gson;
import com.jpardogo.android.googleprogressbar.library.ChromeFloatingCirclesDrawable;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.Types.BoomType;
import com.nightonke.boommenu.Types.ButtonType;
import com.nightonke.boommenu.Types.DimType;
import com.nightonke.boommenu.Types.PlaceType;
import com.nightonke.boommenu.Util;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.view.ViewHelper;
import com.pam.plbtw_g.Adapter.ListKontenAdapter;
import com.pam.plbtw_g.Fragment.ChallengeFragment;
import com.pam.plbtw_g.Fragment.FragmentLagu;
import com.pam.plbtw_g.Fragment.FragmentMainan;
import com.pam.plbtw_g.Fragment.RewardsFragment;
import com.pam.plbtw_g.Model.Konten;
import com.pam.plbtw_g.Model.ResultAPI;
import com.pam.plbtw_g.Rest.RestClient;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import io.codetail.animation.SupportAnimator;
import io.codetail.animation.ViewAnimationUtils;
import io.codetail.animation.arcanimator.ArcAnimator;
import io.codetail.animation.arcanimator.Side;
import io.saeid.fabloading.LoadingView;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

public class MenuUtama extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    ViewPager viewPager;
    TabLayout tabLayout;
    private View mCustomView;
    private BoomMenuButton boomMenuButtonInActionBar;
    private Uri fileUri;
    View mParent;
    FloatingActionButton mBlue;
    ImageView imgKonten;
    private String timeStamp;
    FrameLayout mBluePair;
    String encodedImage;
    public static int COUNT_CAMERA = 100;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final String IMAGE_DIRECTORY_NAME = "MyQueue_CapturedPhoto";
    FloatingActionButton mRed;

    float startBlueX;
    float startBlueY;

    int endBlueX;
    int endBlueY;

    float startRedX;
    float startRedY;

    int startBluePairBottom;
    Call<ResultAPI<Konten>> call;
    RestClient.GitApiInterface service;
    final static AccelerateInterpolator ACCELERATE = new AccelerateInterpolator();
    final static AccelerateDecelerateInterpolator ACCELERATE_DECELERATE = new AccelerateDecelerateInterpolator();
    final static DecelerateInterpolator DECELERATE = new DecelerateInterpolator();
    CircularProgressButton btntambahloading, btnbatal,btngambar;
    private LoadingView mLoadingView;
    private LoadingView mLoadViewLong;
    private String iduser;
    private EditText namakonten,deskripsikonten,lokasikonten;
    private Spinner spinnertag;
    private Button btntambah;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_utama);
       // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
       // setSupportActionBar(toolbar);
        mParent=(View)findViewById(R.id.reve);
        mLoadingView = (LoadingView) findViewById(R.id.loading_view);
        int marvel_1 = R.drawable.ic_add_black_24dp1;
        int marvel_2 = R.drawable.ic_add_black_24dp1;
        int marvel_3 = R.drawable.ic_add_black_24dp1;
        int marvel_4 = R.drawable.ic_add_black_24dp1;
        imgKonten = (ImageView)findViewById(R.id.imgkonten);
        btntambahloading=(CircularProgressButton)findViewById(R.id.btntambahloading);
        btnbatal=(CircularProgressButton)findViewById(R.id.btncancel);
        btngambar=(CircularProgressButton)findViewById(R.id.btngambar);
        SharedPreferences sp=getApplicationContext().getSharedPreferences("MyShared", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor=sp.edit();

        iduser = sp.getString("id_user","");
        mLoadingView.addAnimation(Color.parseColor("#FFD200"),marvel_1,
                LoadingView.FROM_LEFT);
        mLoadingView.addAnimation(Color.parseColor("#2F5DA9"),marvel_2,
                LoadingView.FROM_TOP);
        mLoadingView.addAnimation(Color.parseColor("#FF4218"),marvel_3,
                LoadingView.FROM_RIGHT);
        mLoadingView.addAnimation(Color.parseColor("#C7E7FB"), marvel_4,
                LoadingView.FROM_BOTTOM);

        btngambar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
        btntambahloading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!btntambahloading.isIndeterminateProgressMode())
                {
                    btntambahloading.setIndeterminateProgressMode(true);
                    btntambahloading.setProgress(50);
                    uploads();
                }

            }
        });
        mLoadingView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLoadingView.startAnimation();
                adddata();
            }
        });
        mLoadingView.addListener(new LoadingView.LoadingListener() {
            @Override public void onAnimationStart(int currentItemPosition) {

            }

            @Override public void onAnimationRepeat(int nextItemPosition) {

            }

            @Override public void onAnimationEnd(int nextItemPosition) {

            }
        });
        btnbatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                disappearBluePair();
            }
        });
        namakonten=(EditText) findViewById(R.id.txtnama);
        deskripsikonten=(EditText) findViewById(R.id.txtdeskripsi);
        spinnertag=(Spinner) findViewById(R.id.spinnertag);
        lokasikonten=(EditText) findViewById(R.id.txtlokasi);
        btntambah=(Button) findViewById(R.id.btntambah);
        mBlue = (FloatingActionButton) findViewById(R.id.transition_blue);
        mBluePair = (FrameLayout) findViewById(R.id.transition_blue_pair);
        mRed = (FloatingActionButton) findViewById(R.id.transition_red);
        mBlue.setOnClickListener(mClicker);
        mRed.setOnClickListener(mRClicker);

        TypefaceProvider.registerDefaultIconSets();

        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setElevation(0);
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);
        mTitleTextView.setText(R.string.app_name);



        boomMenuButtonInActionBar = (BoomMenuButton) mCustomView.findViewById(R.id.boom);

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        ((Toolbar) mCustomView.getParent()).setContentInsetsAbsolute(0,0);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.WHITE);
        tabLayout.setSelectedTabIndicatorHeight(10);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

       // final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
      //  ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
      //          this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
      //  drawer.setDrawerListener(toggle);
      //  toggle.syncState();

      //  NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
      //  navigationView.setNavigationItemSelectedListener(this);


    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == COUNT_CAMERA) {

                Bitmap bitmap;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(fileUri.getPath(), options);
                final int REQUIRED_SIZE = 450;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bitmap =  BitmapFactory.decodeFile(fileUri.getPath(), options);
                imgKonten.setVisibility(View.VISIBLE);
                imgKonten.setImageBitmap(bitmap);
                imgKonten.setScaleType(ImageView.ScaleType.FIT_XY);

            } else if (requestCode == 2) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(this, selectedImageUri, projection, null, null,
                        null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();

                String selectedImagePath = cursor.getString(column_index);

                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 450;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(selectedImagePath, options);

                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int width = size.x;

                if(bm.getWidth()<width){
                    int heighttoAdd = bm.getHeight()+(width-bm.getWidth());
                    Bitmap newResizedBitmap = getResizedBitmap(bm,width,heighttoAdd);
                    imgKonten.setVisibility(View.VISIBLE);
                    imgKonten.setImageBitmap(newResizedBitmap);
                }else{
                    imgKonten.setVisibility(View.VISIBLE);
                    imgKonten.setImageBitmap(bm);
                    imgKonten.setScaleType(ImageView.ScaleType.FIT_START);
                }
            }
        }
    }
    public void adddata()
    {
        generateTimeStamp();
        String namaPhoto = timeStamp+"_CulturePedia"+iduser+".jpg";
        service = RestClient.getClient();
        call = service.adddata(namakonten.getText().toString(),deskripsikonten.getText().toString(),namaPhoto,spinnertag.getSelectedItem().toString(),
                lokasikonten.getText().toString(),iduser,encodedImage);

        call.enqueue(new Callback<ResultAPI<Konten>>() {
            @Override
            public void onResponse(Response<ResultAPI<Konten>> response) {
                Log.d("MainActivity", "Status Code = " + response.code());
                if (response.isSuccess()) {
                    btntambahloading.setProgress(100);
                    disappearBluePair();
                    namakonten.setText("");
                    deskripsikonten.setText("");
                    lokasikonten.setText("");
                    Toast.makeText(MenuUtama.this, "Berhasil Menambah Data!",
                            Toast.LENGTH_LONG).show();
                } else {
                    Log.d("MainActivity", "error");
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("on Failure", t.toString());
            }
        });
    }
    View.OnClickListener mRClicker = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            disappearRed();
        }
    };

    View.OnClickListener mClicker = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            btntambahloading.setProgress(0);
            btntambahloading.setIndeterminateProgressMode(false);
            startBlueX = Utils.centerX(mBlue);
            startBlueY = Utils.centerY(mBlue);

            endBlueX = mParent.getRight()/2;
            endBlueY = (int) (mParent.getBottom()/3);//*0.8f);
            ArcAnimator arcAnimator = ArcAnimator.createArcAnimator(mBlue, endBlueX,
                    endBlueY, 90, Side.LEFT)
                    .setDuration(500);
            arcAnimator.addListener(new SimpleListener(){
                @Override
                public void onAnimationEnd(Animator animation) {
                    mBlue.setVisibility(View.INVISIBLE);
                    imgKonten.setVisibility(View.GONE);
                    appearBluePair();
                }
            });
            arcAnimator.start();
        }
    };


    void appearBluePair(){
        mBluePair.setVisibility(View.VISIBLE);

        float finalRadius = Math.max(mBluePair.getWidth(), mBluePair.getHeight()) * 1.5f;

        SupportAnimator animator = ViewAnimationUtils.createCircularReveal(mBluePair, endBlueX, endBlueY, mBlue.getWidth() / 2f,
                finalRadius);
        animator.setDuration(500);
        animator.setInterpolator(ACCELERATE);
        animator.addListener(new SimpleListener(){
            @Override
            public void onAnimationEnd() {
                //raise();
                startBluePairBottom = mBluePair.getBottom();
                //appearRed();
            }
        });
        animator.start();
    }
    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library", "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(MenuUtama.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                    startActivityForResult(intent, COUNT_CAMERA);
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            2);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private File getOutputMediaFile(int type) {
        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        // Create a media file name
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            generateTimeStamp();
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp +"_user3.jpg");
        } else {
            return null;
        }
        MediaScannerConnection.scanFile(this, new String[]{Environment.getExternalStorageDirectory().toString()},
                null, new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Log.i("ExternalStorage", "Scanned " + path + ":");
                        Log.i("ExternalStorage", "-> uri=" + uri);
                    }
                });
        return mediaFile;
    }
    private void generateTimeStamp(){ // KALO MW GENERATE WAKTU
        timeStamp= new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
    }
    public void uploads(){


        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ((BitmapDrawable) imgKonten.getDrawable()).getBitmap().compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
        adddata();



    }
    void raise(){
        startBluePairBottom = mBluePair.getBottom();
        ObjectAnimator objectAnimator = ObjectAnimator.ofInt(mBluePair, "bottom", mBluePair.getBottom(), mBluePair.getTop() + dpToPx(100));
        objectAnimator.addListener(new SimpleListener(){
            @Override
            public void onAnimationEnd(Animator animation) {
                appearRed();
            }
        });
        objectAnimator.setInterpolator(ACCELERATE_DECELERATE);
        objectAnimator.start();
    }

    void appearRed(){
        mRed.setVisibility(View.VISIBLE);

        int cx = mRed.getWidth() / 2;
        int cy = mRed.getHeight() / 2;

        SupportAnimator animator = ViewAnimationUtils.createCircularReveal(mRed,cx, cy, 0, mRed.getWidth()/2);
        animator.addListener(new SimpleListener(){
            @Override
            public void onAnimationEnd() {
                upRed();
            }
        });
        animator.setInterpolator(ACCELERATE);
        animator.start();
    }

    void upRed(){
        startRedX = ViewHelper.getX(mRed);
        startRedY = ViewHelper.getY(mRed);
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(mRed, "y", ViewHelper.getY(mRed),
                mBluePair.getTop()+50);
        objectAnimator.addListener(new SimpleListener(){
            @Override
            public void onAnimationEnd(Animator animation) {
                //disappearRed();
            }
        });
        objectAnimator.setDuration(650);
        objectAnimator.setInterpolator(ACCELERATE_DECELERATE);
        objectAnimator.start();
    }

    void disappearRed(){

        int cx = mRed.getWidth() / 2;
        int cy = mRed.getHeight() / 2;

        SupportAnimator animator = ViewAnimationUtils.createCircularReveal(mRed,cx, cy, mRed.getWidth()/2, 0);
        animator.addListener(new SimpleListener(){
            @Override
            public void onAnimationEnd() {
                mRed.setVisibility(View.INVISIBLE);
                ViewHelper.setX(mRed,startRedX);
                ViewHelper.setY(mRed,startRedY);
                release();
            }
        });
        animator.setInterpolator(DECELERATE);
        animator.start();
    }

    void release(){
        ObjectAnimator objectAnimator = ObjectAnimator.ofInt(mBluePair, "bottom", mBluePair.getBottom(),startBluePairBottom);
        Log.d("BACA"," "+startBluePairBottom);
        objectAnimator.addListener(new SimpleListener(){
            @Override
            public void onAnimationEnd(Animator animator) {
                disappearBluePair();
            }
        });
        objectAnimator.setInterpolator(ACCELERATE_DECELERATE);
        objectAnimator.start();
    }

    void disappearBluePair(){
        float finalRadius = Math.max(mBluePair.getWidth(), mBluePair.getHeight()) * 1.5f;

        SupportAnimator animator = ViewAnimationUtils.createCircularReveal(mBluePair, endBlueX, endBlueY,
                finalRadius, mBlue.getWidth() / 2f);
        animator.setDuration(500);
        animator.addListener(new SimpleListener() {
            @Override
            public void onAnimationEnd() {
                mBluePair.setVisibility(View.INVISIBLE);
                returnBlue();
            }
        });
        animator.setInterpolator(DECELERATE);
        animator.start();
    }

    void returnBlue(){
        mBlue.setVisibility(View.VISIBLE);
        ArcAnimator arcAnimator = ArcAnimator.createArcAnimator(mBlue, startBlueX,
                startBlueY, 90, Side.LEFT)
                .setDuration(500);
        arcAnimator.start();

    }

    public int dpToPx(int dp){
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }


    private static class SimpleListener implements SupportAnimator.AnimatorListener, ObjectAnimator.AnimatorListener{

        @Override
        public void onAnimationStart() {

        }

        @Override
        public void onAnimationEnd() {

        }

        @Override
        public void onAnimationCancel() {

        }

        @Override
        public void onAnimationRepeat() {

        }

        @Override
        public void onAnimationStart(Animator animation) {

        }

        @Override
        public void onAnimationEnd(Animator animation) {

        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentMainan(), "Mainan");
        adapter.addFragment(new FragmentLagu(), "Lagu");
        viewPager.setAdapter(adapter);

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_utama, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        String title = null;
        if (id == R.id.nav_home) {
            // Handle the camera action
                tabLayout.setVisibility(View.VISIBLE);
                viewPager.setVisibility(View.VISIBLE);
                title="Home";
        } else if (id == R.id.nav_profil) {
                tabLayout.setVisibility(View.GONE);
                viewPager.setVisibility(View.GONE);
                title="Profil";
        } else if (id == R.id.nav_chalenge) {
                tabLayout.setVisibility(View.GONE);
                viewPager.setVisibility(View.GONE);
                title="Challenge";
        } else if (id == R.id.nav_reward) {
                tabLayout.setVisibility(View.GONE);
                viewPager.setVisibility(View.GONE);
                title="Reward";
        }
        getSupportActionBar().setTitle(title);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        int number=4;
        Drawable[] drawables = new Drawable[number];
        int[] drawablesResource = new int[]{
                R.drawable.ic_action_home,
                R.drawable.chalenge_ic,
                R.drawable.rewards_ic,
                R.drawable.logout_ic
        };
        for (int i = 0; i < number; i++)
            drawables[i] = ContextCompat.getDrawable(this, drawablesResource[i]);

        String[] STRINGS = new String[]{
                "Home",
                "Challenge",
                "Rewards",
                "Logout"
        };
        String[] strings = new String[number];
        for (int i = 0; i < number; i++)
            strings[i] = STRINGS[i];

        int[][] colors = new int[number][2];
        for (int i = 0; i < number; i++) {
            colors[0][1] = Color.parseColor("#3E2723");
            colors[1][1] = Color.parseColor("#3E2723");
            colors[2][1] = Color.parseColor("#3E2723");
            colors[3][1] = Color.parseColor("#3E2723");
            colors[i][0] = Util.getInstance().getPressedColor(colors[i][1]);
        }

        ButtonType buttonType = ButtonType.CIRCLE;

        new BoomMenuButton.Builder()
                .subButtons(drawables, colors, strings)
                .button(buttonType)
                .boom(BoomType.PARABOLA)
                .place(PlaceType.CIRCLE_4_1)
                .subButtonsShadow(Util.getInstance().dp2px(2), Util.getInstance().dp2px(2))
                .subButtonsShadow(Util.getInstance().dp2px(2), Util.getInstance().dp2px(2))
                .onSubButtonClick(new BoomMenuButton.OnSubButtonClickListener() {
                    @Override
                    public void onClick(int buttonIndex) {
                        Fragment fragment = null;
                        FrameLayout con = (FrameLayout) findViewById(R.id.container_body);
                        TextView judul = (TextView)findViewById(R.id.title_text);
                        mBlue.setVisibility(View.GONE);
                        String title = null;
                        if(buttonIndex==0)
                        {
                            con.setVisibility(View.GONE);
                            mBlue.setVisibility(View.VISIBLE);
                            tabLayout.setVisibility(View.VISIBLE);
                            viewPager.setVisibility(View.VISIBLE);
                            title="Home";
                        }
                        else if(buttonIndex==1)
                        {
                            con.setVisibility(View.VISIBLE);

                            tabLayout.setVisibility(View.GONE);
                            viewPager.setVisibility(View.GONE);
                            title="Challenge";
                            fragment=new ChallengeFragment();
                        }
                        else if(buttonIndex==2)
                        {
                            con.setVisibility(View.VISIBLE);

                            tabLayout.setVisibility(View.GONE);
                            viewPager.setVisibility(View.GONE);
                            title="Rewards";
                            fragment=new RewardsFragment();
                        }
                        if(buttonIndex==3)
                        {
                            new AlertDialog.Builder(MenuUtama.this)
                                    .setTitle("Log Out")
                                    .setMessage("Apa anda yakin ingin melakukan log out?")
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            SharedPreferences sp=getApplicationContext().getSharedPreferences("MyShared", Activity.MODE_PRIVATE);
                                            SharedPreferences.Editor editor=sp.edit();
                                            editor.putString("session","");
                                            editor.apply();
                                            Intent i=new Intent(MenuUtama.this,login.class);
                                            startActivity(i);
                                            finish();
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {

                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        }
                        if(fragment!=null) {
                            FragmentManager fragmentManager = getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container_body, fragment);
                            fragmentTransaction.commit();

                        }
                        judul.setText(title);
                    }
                })
                .dim(DimType.DIM_0)
                .init(boomMenuButtonInActionBar);
    }
    private String[] Colors = {
            "#F44336",
            "#E91E63",
            "#9C27B0",
            "#2196F3",
            "#03A9F4",
            "#00BCD4",
            "#009688",
            "#4CAF50",
            "#8BC34A",
            "#CDDC39",
            "#FFEB3B",
            "#FFC107",
            "#FF9800",
            "#FF5722",
            "#795548",
            "#9E9E9E",
            "#607D8B"};

    public int GetRandomColor() {
        Random random = new Random();
        int p = random.nextInt(Colors.length);
        return Color.parseColor(Colors[p]);
    }
}
