package com.pam.plbtw_g;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.TypefaceProvider;
import com.google.gson.Gson;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.Types.BoomType;
import com.nightonke.boommenu.Types.ButtonType;
import com.nightonke.boommenu.Types.DimType;
import com.nightonke.boommenu.Types.PlaceType;
import com.nightonke.boommenu.Util;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.view.ViewHelper;
import com.pam.plbtw_g.Adapter.ListKontenAdapter;
import com.pam.plbtw_g.Fragment.ChallengeFragment;
import com.pam.plbtw_g.Fragment.FragmentLagu;
import com.pam.plbtw_g.Fragment.FragmentMainan;
import com.pam.plbtw_g.Model.Konten;
import com.pam.plbtw_g.Model.ResultAPI;
import com.pam.plbtw_g.Rest.RestClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.codetail.animation.SupportAnimator;
import io.codetail.animation.ViewAnimationUtils;
import io.codetail.animation.arcanimator.ArcAnimator;
import io.codetail.animation.arcanimator.Side;
import io.saeid.fabloading.LoadingView;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

public class MenuUtama extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    ViewPager viewPager;
    TabLayout tabLayout;
    private View mCustomView;
    private BoomMenuButton boomMenuButtonInActionBar;

    View mParent;
    FloatingActionButton mBlue;
    FrameLayout mBluePair;

    FloatingActionButton mRed;

    float startBlueX;
    float startBlueY;

    int endBlueX;
    int endBlueY;

    float startRedX;
    float startRedY;

    int startBluePairBottom;
    Call<ResultAPI<Konten>> call;
    RestClient.GitApiInterface service;
    final static AccelerateInterpolator ACCELERATE = new AccelerateInterpolator();
    final static AccelerateDecelerateInterpolator ACCELERATE_DECELERATE = new AccelerateDecelerateInterpolator();
    final static DecelerateInterpolator DECELERATE = new DecelerateInterpolator();

    private LoadingView mLoadingView;
    private LoadingView mLoadViewLong;

    private EditText namakonten,deskripsikonten,lokasikonten;
    private Spinner spinnertag;
    private Button btntambah;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_utama);
       // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
       // setSupportActionBar(toolbar);
        mParent=(View)findViewById(R.id.reve);
        mLoadingView = (LoadingView) findViewById(R.id.loading_view);
        int marvel_1 = R.drawable.ic_add_black_24dp1;
        int marvel_2 = R.drawable.ic_add_black_24dp1;
        int marvel_3 = R.drawable.ic_add_black_24dp1;
        int marvel_4 = R.drawable.ic_add_black_24dp1;


        mLoadingView.addAnimation(Color.parseColor("#FFD200"),marvel_1,
                LoadingView.FROM_LEFT);
        mLoadingView.addAnimation(Color.parseColor("#2F5DA9"),marvel_2,
                LoadingView.FROM_TOP);
        mLoadingView.addAnimation(Color.parseColor("#FF4218"),marvel_3,
                LoadingView.FROM_RIGHT);
        mLoadingView.addAnimation(Color.parseColor("#C7E7FB"), marvel_4,
                LoadingView.FROM_BOTTOM);


        mLoadingView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLoadingView.startAnimation();
                adddata();
            }
        });
        mLoadingView.addListener(new LoadingView.LoadingListener() {
            @Override public void onAnimationStart(int currentItemPosition) {

            }

            @Override public void onAnimationRepeat(int nextItemPosition) {

            }

            @Override public void onAnimationEnd(int nextItemPosition) {

            }
        });

        namakonten=(EditText) findViewById(R.id.txtnama);
        deskripsikonten=(EditText) findViewById(R.id.txtdeskripsi);
        spinnertag=(Spinner) findViewById(R.id.spinnertag);
        lokasikonten=(EditText) findViewById(R.id.txtlokasi);
        btntambah=(Button) findViewById(R.id.btntambah);
        mBlue = (FloatingActionButton) findViewById(R.id.transition_blue);
        mBluePair = (FrameLayout) findViewById(R.id.transition_blue_pair);
        mRed = (FloatingActionButton) findViewById(R.id.transition_red);
        mBlue.setOnClickListener(mClicker);
        mRed.setOnClickListener(mRClicker);

        TypefaceProvider.registerDefaultIconSets();

        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setElevation(0);
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);
        mTitleTextView.setText(R.string.app_name);



        boomMenuButtonInActionBar = (BoomMenuButton) mCustomView.findViewById(R.id.boom);

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        ((Toolbar) mCustomView.getParent()).setContentInsetsAbsolute(0,0);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.WHITE);
        tabLayout.setSelectedTabIndicatorHeight(10);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

       // final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
      //  ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
      //          this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
      //  drawer.setDrawerListener(toggle);
      //  toggle.syncState();

      //  NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
      //  navigationView.setNavigationItemSelectedListener(this);


    }
    public void adddata()
    {
        service = RestClient.getClient();
        call = service.adddata(namakonten.getText().toString(),deskripsikonten.getText().toString(),"tes",spinnertag.getSelectedItem().toString(),
                lokasikonten.getText().toString(),"8");

        call.enqueue(new Callback<ResultAPI<Konten>>() {
            @Override
            public void onResponse(Response<ResultAPI<Konten>> response) {
                //progressimage.setVisibility(View.GONE);
                //notfound.setVisibility(View.GONE);
                Log.d("MainActivity", "Status Code = " + response.code());
                if (response.isSuccess()) {
                    // request successful (status code 200, 201)
                    disappearRed();
                    mLoadingView.pauseAnimation();
                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors
                    Log.d("MainActivity", "error");
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("on Failure", t.toString());
                //Progress.setVisibility(View.GONE);
                //progressimage.setVisibility(View.GONE);
                //notfound.setVisibility(View.GONE);
                Log.d("MainActivity", "gagalall");
                //Toast.makeText(v.getContext(), "Cek Koneksi Anda", Toast.LENGTH_LONG).show();
            }
        });
    }
    View.OnClickListener mRClicker = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            disappearRed();
        }
    };

    View.OnClickListener mClicker = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startBlueX = Utils.centerX(mBlue);
            startBlueY = Utils.centerY(mBlue);

            endBlueX = mParent.getRight()/2;
            endBlueY = (int) (mParent.getBottom()/3);//*0.8f);
            ArcAnimator arcAnimator = ArcAnimator.createArcAnimator(mBlue, endBlueX,
                    endBlueY, 90, Side.LEFT)
                    .setDuration(500);
            arcAnimator.addListener(new SimpleListener(){
                @Override
                public void onAnimationEnd(Animator animation) {
                    mBlue.setVisibility(View.INVISIBLE);
                    appearBluePair();
                }
            });
            arcAnimator.start();
        }
    };


    void appearBluePair(){
        mBluePair.setVisibility(View.VISIBLE);

        float finalRadius = Math.max(mBluePair.getWidth(), mBluePair.getHeight()) * 1.5f;

        SupportAnimator animator = ViewAnimationUtils.createCircularReveal(mBluePair, endBlueX, endBlueY, mBlue.getWidth() / 2f,
                finalRadius);
        animator.setDuration(500);
        animator.setInterpolator(ACCELERATE);
        animator.addListener(new SimpleListener(){
            @Override
            public void onAnimationEnd() {
                //raise();
                startBluePairBottom = mBluePair.getBottom();
                appearRed();
            }
        });
        animator.start();
    }

    void raise(){
        startBluePairBottom = mBluePair.getBottom();
        ObjectAnimator objectAnimator = ObjectAnimator.ofInt(mBluePair, "bottom", mBluePair.getBottom(), mBluePair.getTop() + dpToPx(100));
        objectAnimator.addListener(new SimpleListener(){
            @Override
            public void onAnimationEnd(Animator animation) {
                appearRed();
            }
        });
        objectAnimator.setInterpolator(ACCELERATE_DECELERATE);
        objectAnimator.start();
    }

    void appearRed(){
        mRed.setVisibility(View.VISIBLE);

        int cx = mRed.getWidth() / 2;
        int cy = mRed.getHeight() / 2;

        SupportAnimator animator = ViewAnimationUtils.createCircularReveal(mRed,cx, cy, 0, mRed.getWidth()/2);
        animator.addListener(new SimpleListener(){
            @Override
            public void onAnimationEnd() {
                upRed();
            }
        });
        animator.setInterpolator(ACCELERATE);
        animator.start();
    }

    void upRed(){
        startRedX = ViewHelper.getX(mRed);
        startRedY = ViewHelper.getY(mRed);
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(mRed, "y", ViewHelper.getY(mRed),
                800);
        objectAnimator.addListener(new SimpleListener(){
            @Override
            public void onAnimationEnd(Animator animation) {
                //disappearRed();
            }
        });
        objectAnimator.setDuration(650);
        objectAnimator.setInterpolator(ACCELERATE_DECELERATE);
        objectAnimator.start();
    }

    void disappearRed(){

        int cx = mRed.getWidth() / 2;
        int cy = mRed.getHeight() / 2;

        SupportAnimator animator = ViewAnimationUtils.createCircularReveal(mRed,cx, cy, mRed.getWidth()/2, 0);
        animator.addListener(new SimpleListener(){
            @Override
            public void onAnimationEnd() {
                mRed.setVisibility(View.INVISIBLE);
                ViewHelper.setX(mRed,startRedX);
                ViewHelper.setY(mRed,startRedY);
                release();
            }
        });
        animator.setInterpolator(DECELERATE);
        animator.start();
    }

    void release(){
        ObjectAnimator objectAnimator = ObjectAnimator.ofInt(mBluePair, "bottom", mBluePair.getBottom(),startBluePairBottom);
        Log.d("BACA"," "+startBluePairBottom);
        objectAnimator.addListener(new SimpleListener(){
            @Override
            public void onAnimationEnd(Animator animator) {
                disappearBluePair();
            }
        });
        objectAnimator.setInterpolator(ACCELERATE_DECELERATE);
        objectAnimator.start();
    }

    void disappearBluePair(){
        float finalRadius = Math.max(mBluePair.getWidth(), mBluePair.getHeight()) * 1.5f;

        SupportAnimator animator = ViewAnimationUtils.createCircularReveal(mBluePair, endBlueX, endBlueY,
                finalRadius, mBlue.getWidth() / 2f);
        animator.setDuration(500);
        animator.addListener(new SimpleListener() {
            @Override
            public void onAnimationEnd() {
                mBluePair.setVisibility(View.INVISIBLE);
                returnBlue();
            }
        });
        animator.setInterpolator(DECELERATE);
        animator.start();
    }

    void returnBlue(){
        mBlue.setVisibility(View.VISIBLE);
        ArcAnimator arcAnimator = ArcAnimator.createArcAnimator(mBlue, startBlueX,
                startBlueY, 90, Side.LEFT)
                .setDuration(500);
        arcAnimator.start();

    }

    public int dpToPx(int dp){
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }


    private static class SimpleListener implements SupportAnimator.AnimatorListener, ObjectAnimator.AnimatorListener{

        @Override
        public void onAnimationStart() {

        }

        @Override
        public void onAnimationEnd() {

        }

        @Override
        public void onAnimationCancel() {

        }

        @Override
        public void onAnimationRepeat() {

        }

        @Override
        public void onAnimationStart(Animator animation) {

        }

        @Override
        public void onAnimationEnd(Animator animation) {

        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentMainan(), "Mainan");
        adapter.addFragment(new FragmentLagu(), "Lagu");
        viewPager.setAdapter(adapter);

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_utama, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        String title = null;
        if (id == R.id.nav_home) {
            // Handle the camera action
                tabLayout.setVisibility(View.VISIBLE);
                viewPager.setVisibility(View.VISIBLE);
                title="Home";
        } else if (id == R.id.nav_profil) {
                tabLayout.setVisibility(View.GONE);
                viewPager.setVisibility(View.GONE);
                title="Profil";
        } else if (id == R.id.nav_chalenge) {
                tabLayout.setVisibility(View.GONE);
                viewPager.setVisibility(View.GONE);
                title="Challenge";
        } else if (id == R.id.nav_reward) {
                tabLayout.setVisibility(View.GONE);
                viewPager.setVisibility(View.GONE);
                title="Reward";
        }
        getSupportActionBar().setTitle(title);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        int number=5;
        Drawable[] drawables = new Drawable[number];
        int[] drawablesResource = new int[]{
                R.drawable.ic_action_home,
                R.drawable.profile_ic,
                R.drawable.chalenge_ic,
                R.drawable.rewards_ic,
                R.drawable.logout_ic
        };
        for (int i = 0; i < number; i++)
            drawables[i] = ContextCompat.getDrawable(this, drawablesResource[i]);

        String[] STRINGS = new String[]{
                "Home",
                "Profile",
                "Challenge",
                "Rewards",
                "Logout"
        };
        String[] strings = new String[number];
        for (int i = 0; i < number; i++)
            strings[i] = STRINGS[i];

        int[][] colors = new int[number][2];
        for (int i = 0; i < number; i++) {
            colors[0][1] = Color.parseColor("#3399ff");
            colors[1][1] = Color.parseColor("#3399ff");
            colors[2][1] = Color.parseColor("#3399ff");
            colors[3][1] = Color.parseColor("#ffa31a");
            colors[4][1] = Color.parseColor("#ffa31a");
            colors[i][0] = Util.getInstance().getPressedColor(colors[i][1]);
        }

        ButtonType buttonType = ButtonType.CIRCLE;

        new BoomMenuButton.Builder()
                .subButtons(drawables, colors, strings)
                .button(buttonType)
                .boom(BoomType.PARABOLA)
                .place(PlaceType.CIRCLE_5_3)
                .subButtonsShadow(Util.getInstance().dp2px(2), Util.getInstance().dp2px(2))
                .subButtonsShadow(Util.getInstance().dp2px(2), Util.getInstance().dp2px(2))
                .onSubButtonClick(new BoomMenuButton.OnSubButtonClickListener() {
                    @Override
                    public void onClick(int buttonIndex) {
                        Fragment fragment = null;
                        FrameLayout con = (FrameLayout) findViewById(R.id.container_body);
                        TextView judul = (TextView)findViewById(R.id.title_text);
                        mBlue.setVisibility(View.GONE);
                        String title = null;
                        if(buttonIndex==0)
                        {
                            con.setVisibility(View.GONE);
                            mBlue.setVisibility(View.VISIBLE);
                            tabLayout.setVisibility(View.VISIBLE);
                            viewPager.setVisibility(View.VISIBLE);
                            title="Home";
                        }
                        else if(buttonIndex==1)
                        {
                            tabLayout.setVisibility(View.GONE);
                            viewPager.setVisibility(View.GONE);
                            title="Profil";
                        }
                        else if(buttonIndex==2)
                        {
                            con.setVisibility(View.VISIBLE);

                            tabLayout.setVisibility(View.GONE);
                            viewPager.setVisibility(View.GONE);
                            title="Challenge";
                            fragment=new ChallengeFragment();
                        }
                        if(fragment!=null) {
                            FragmentManager fragmentManager = getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container_body, fragment);
                            fragmentTransaction.commit();

                        }
                        judul.setText(title);
                    }
                })
                .dim(DimType.DIM_0)
                .init(boomMenuButtonInActionBar);
    }
    private String[] Colors = {
            "#F44336",
            "#E91E63",
            "#9C27B0",
            "#2196F3",
            "#03A9F4",
            "#00BCD4",
            "#009688",
            "#4CAF50",
            "#8BC34A",
            "#CDDC39",
            "#FFEB3B",
            "#FFC107",
            "#FF9800",
            "#FF5722",
            "#795548",
            "#9E9E9E",
            "#607D8B"};

    public int GetRandomColor() {
        Random random = new Random();
        int p = random.nextInt(Colors.length);
        return Color.parseColor(Colors[p]);
    }
}
