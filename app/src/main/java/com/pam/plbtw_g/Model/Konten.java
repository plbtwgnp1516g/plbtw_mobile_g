package com.pam.plbtw_g.Model;

/**
 * Created by PIER on 5/25/2016.
 */
public class Konten {
    public String getId_konten() {
        return id_konten;
    }

    public void setId_konten(String id_konten) {
        this.id_konten = id_konten;
    }

    String id_konten;
    String id_user;
    String nama_konten;
    String deskripsi_konten;
    String gambar_konten;
    String tag_konten;
    String lokasi_konten;
    String total_rate;


    String Flag_rate;

    public String getFlag_rate() {
        return Flag_rate;
    }

    public void setFlag_rate(String flag_rate) {
        this.Flag_rate = flag_rate;
    }


    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getNama_konten() {
        return nama_konten;
    }

    public void setNama_konten(String nama_konten) {
        this.nama_konten = nama_konten;
    }

    public String getDeskripsi_konten() {
        return deskripsi_konten;
    }

    public void setDeskripsi_konten(String deskripsi_konten) {
        this.deskripsi_konten = deskripsi_konten;
    }

    public String getGambar_konten() {
        return gambar_konten;
    }

    public void setGambar_konten(String gambar_konten) {
        this.gambar_konten = gambar_konten;
    }

    public String getTag_konten() {
        return tag_konten;
    }

    public void setTag_konten(String tag_konten) {
        this.tag_konten = tag_konten;
    }

    public String getLokasi_konten() {
        return lokasi_konten;
    }

    public void setLokasi_konten(String lokasi_konten) {
        this.lokasi_konten = lokasi_konten;
    }

    public String getTotal_rate() {
        return total_rate;
    }

    public void setTotal_rate(String total_rate) {
        this.total_rate = total_rate;
    }


}
