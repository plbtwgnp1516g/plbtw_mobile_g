package com.pam.plbtw_g.Model;

/**
 * Created by PIER on 5/25/2016.
 */
import java.util.ArrayList;


public class ResultAPI<T> {
    private int status;

    public String getTotalRates() {
        return totalRates;
    }

    public void setTotalRates(String totalRates) {
        this.totalRates = totalRates;
    }

    private String totalRates;




    private ArrayList<T> dataKonten = new ArrayList<T>();

    public ArrayList<T> getUserData() {
        return userData;
    }

    public void setUserData(ArrayList<T> userData) {
        this.userData = userData;
    }

    private ArrayList<T> userData = new ArrayList<T>();

    public ArrayList<T> getDataKonten() {
        return dataKonten;
    }

    public void setDataKonten(ArrayList<T> dataKonten) {
        this.dataKonten = dataKonten;
    }

    public ArrayList<T> getHistoryData() {
        return historyData;
    }

    public void setHistoryData(ArrayList<T> historyData) {
        this.historyData = historyData;
    }

    public String getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(String totalPoints) {
        this.totalPoints = totalPoints;
    }

    private String totalPoints;
    private ArrayList<T> historyData = new ArrayList<T>();

    public int getSukses() {
        return status;
    }

    public void setSukses(int status) {
        this.status = status;
    }
}
