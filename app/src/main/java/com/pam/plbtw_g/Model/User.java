package com.pam.plbtw_g.Model;

/**
 * Created by PIER on 6/3/2016.
 */
public class User {

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getPoints_total() {
        return points_total;
    }

    public void setPoints_total(String points_total) {
        this.points_total = points_total;
    }

    public String getChallenge_1() {
        return challenge_1;
    }

    public void setChallenge_1(String challenge_1) {
        this.challenge_1 = challenge_1;
    }

    public String getChallenge_2() {
        return challenge_2;
    }

    public void setChallenge_2(String challenge_2) {
        this.challenge_2 = challenge_2;
    }

    public String getChallenge_3() {
        return challenge_3;
    }

    public void setChallenge_3(String challenge_3) {
        this.challenge_3 = challenge_3;
    }

    public String getChallenge_4() {
        return challenge_4;
    }

    public void setChallenge_4(String challenge_4) {
        this.challenge_4 = challenge_4;
    }

    public String getChallenge_5() {
        return challenge_5;
    }

    public void setChallenge_5(String challenge_5) {
        this.challenge_5 = challenge_5;
    }

    private String id_user;
    private String email;
    private String fullname;
    private String password;
    private String points_total;
    private String challenge_1;
    private String challenge_2;
    private String challenge_3;
    private String challenge_4;
    private String challenge_5;

    /**
     *
     * @return
     * The idUser
     */


    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The fullname
     */
    public String getFullname() {
        return fullname;
    }

    /**
     *
     * @param fullname
     * The fullname
     */
    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    /**
     *
     * @return
     * The password
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     * The password
     */
    public void setPassword(String password) {
        this.password = password;
    }




}