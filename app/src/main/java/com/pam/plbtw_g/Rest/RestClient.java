package com.pam.plbtw_g.Rest;

/**
 * Created by PIER on 5/25/2016.
 */
import com.pam.plbtw_g.Helper.ToStringConverter;
import com.pam.plbtw_g.Model.HistoryData;
import com.pam.plbtw_g.Model.Konten;
import com.pam.plbtw_g.Model.Rate;
import com.pam.plbtw_g.Model.ResultAPI;
import com.pam.plbtw_g.Model.TotalRate;
import com.pam.plbtw_g.Model.User;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;

import java.io.IOException;

import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

public class RestClient {

    private static GitApiInterface gitApiInterface ;
    private static String baseUrl = "http://plbtwtraditionalculture.16mb.com/" ;

    public static GitApiInterface getClient() {
        if (gitApiInterface == null) {

            OkHttpClient okClient = new OkHttpClient();
            okClient.interceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Response response = chain.proceed(chain.request());
                    return response;
                }
            });

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverter(String.class, new ToStringConverter())
                    .client(okClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            gitApiInterface = client.create(GitApiInterface.class);
        }
        return gitApiInterface ;
    }

    public interface GitApiInterface {

     /*   @GET("api/tampil_feed.php")
        Call<ResultAPI<Kabar>> getKabar();

        @FormUrlEncoded
        @POST("api/login.php")
        Call<ResultAPI<User>> login(@Field("username")String  username, @Field("password")String  password);

        @GET("api/tampil_produkbykategori.php")
        Call<ResultAPI<Produk>> getProduk(@Query("kategori") String kategori);

        @FormUrlEncoded
        @POST("api/redeem.php")
        Call<ResultAPI<Redeem>> redeemCode(@Field("email") String kode_voucher,@Field("id_user") String id_user);

        @FormUrlEncoded
        @POST("api/registration.php")
        Call<ResultAPI<User>> registerUser(@Field("username") String username,@Field("password") String password,@Field("nama") String nama,@Field("alamat") String alamat,@Field("no_telp") String no_telp,@Field("email") String email);

        @FormUrlEncoded
        @POST("api/cek_username.php")
        Call<ResultAPI<User>> checkUser(@Field("username") String username);

        @FormUrlEncoded
        @POST("api/update_saldo.php")
        Call<ResultAPI<User>> updateSaldo(@Field("id_user") String id_user, @Field("saldo") String saldo);
        */
         @FormUrlEncoded
         @POST("traditional_culture_api/index.php/api/TraditionalCulture/rateUp/")
         Call<ResultAPI<Rate>> uprate(@Field("id_user")String  id_user, @Field("id_konten")String  id_konten);

        @FormUrlEncoded
        @POST("traditional_culture_api/index.php/api/TraditionalCulture/rateDown/")
        Call<ResultAPI<Rate>> downrate(@Field("id_user")String  id_user, @Field("id_konten")String  id_konten);

        @FormUrlEncoded
        @POST("traditional_culture_api/index.php/api/TraditionalCulture/addKontenTraditional/")
        Call<ResultAPI<Konten>> adddata(@Field("nama_konten")String  nama_konten, @Field("deskripsi_konten")String  deskripsi_konten,
                                        @Field("gambar_konten")String  gambar_konten, @Field("tag_konten")String  tag_konten,
                                        @Field("lokasi_konten")String  lokasi_konten, @Field("id_user")String  id_user,
                                        @Field("gambar")String  gambar);

        @FormUrlEncoded
        @POST("traditional_culture_api/index.php/api/TraditionalCulture/rateNormal/")
        Call<ResultAPI<Rate>> normalrate(@Field("id_user")String  id_user, @Field("id_konten")String  id_konten);

        @GET("traditional_culture_api/index.php/api/TraditionalCulture/kontenAllTraditional/id_user/")
        Call<ResultAPI<Konten>> getKonten(@Query("id_user") String id_user);

        @GET("traditional_culture_api/index.php/api/TraditionalCulture/totalvotes/id/")
        Call<ResultAPI<TotalRate>> getTotalRate(@Query("id") String id);

        @GET("traditional_culture_api/index.php/api/TraditionalCulture/userData/id_user/")
        Call<ResultAPI<User>> getUserData(@Query("id_user") String id_user);

        @GET("traditional_culture_api/index.php/api/TraditionalCulture/getHistoryRedeem/id_user/")
        Call<ResultAPI<HistoryData>> getUserRedeemHistory(@Query("id_user") String id_user);


        @GET("traditional_culture_api/index.php/api/TraditionalCulture/totalpoints/id/")
        Call<ResultAPI> getTotalPoints(@Query("id") String id);

        @FormUrlEncoded
        @POST("traditional_culture_api/index.php/api/TraditionalCulture/redeemChallenge/")
        Call<ResultAPI<Rate>> redeemChallenge(@Field("id_user")String  id_user,
                                              @Field("challengeno")int  challengeno,
                                              @Field("addpoint")int  addpoint);

        @FormUrlEncoded
        @POST("traditional_culture_api/index.php/api/TraditionalCulture/redeemRewards/")
        Call<ResultAPI<Rate>> redeemRewards(@Field("id_user")String  id_user,
                                            @Field("minuspoint")int  minuspoint,
                                            @Field("redeemdate")String  redeemdate,
                                            @Field("productname")String  productname);


    }

}