package com.pam.plbtw_g;

import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.transition.Slide;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

/**
 * Created by Nicolas Juniar on 17/05/2016.
 */
public class deskripsi extends AppCompatActivity {
    private TextView nama,tag,deskripsi;
    private ScrollView scrollView;
    private String nama_konten,deskripsi_konten,tag_konten;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

// inside your activity (if you did not enable transitions in your theme)


        setContentView(R.layout.deskripsi);

        scrollView =(ScrollView)findViewById(R.id.scrollView);

        //this.overridePendingTransition(R.anim.right_to_left,R.anim.left_to_right);
        deskripsi=(TextView)findViewById(R.id.txtDeskripsi);
        tag=(TextView)findViewById(R.id.txttag);
        nama=(TextView)findViewById(R.id.txtnama);

        Bundle b = getIntent().getExtras();
        tag_konten = b.getString("tag_konten");
        nama_konten = b.getString("nama_konten");
        deskripsi_konten = b.getString("deskripsi_konten");

        deskripsi.setText(deskripsi_konten);
        tag.setText(tag_konten);
        nama.setText(nama_konten);

    }
}
