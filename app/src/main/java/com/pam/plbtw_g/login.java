package com.pam.plbtw_g;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.transition.Slide;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class login extends AppCompatActivity {

    ProgressDialog pDialog;
    JSONParser jsonparser;
    EditText email,pass;
    public int sukses;
    private Activity act;
    public boolean cek = false;
    private CoordinatorLayout coordinatorLayout;
    JSONArray names = null;
    private Button btnlogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        btnlogin=(Button)findViewById(R.id.btnLogin) ;
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.login);
        jsonparser=new JSONParser();
        email=(EditText) findViewById(R.id.txtEmail);
        pass=(EditText) findViewById(R.id.txtPass);
        act=this;
    }


    public void Log_in(View v)
    {
        new LoginAsynctask(email.getText().toString(),pass.getText().toString()).execute();
    }

    public void Register(View v)
    {
        Intent i=new Intent(login.this,registerActivity.class);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class LoginAsynctask extends AsyncTask<String, String, String> {
        String Email,Password,FullName;
        int id_user,challenge_1,challenge_2,challenge_3,challenge_4,challenge_5,total_points;

        public LoginAsynctask(String Email,String Password) {
            this.Email = Email;
            this.Password = Password;
            pDialog = new ProgressDialog(login.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setMessage("Mengecek Pengguna....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("email", Email));
            params.add(new BasicNameValuePair("password", Password));
            try {
                JSONObject json = jsonparser.makeHttpRequest("http://plbtwtraditionalculture.16mb.com/traditional_culture_api/index.php/api/TraditionalCulture/login", "POST", params);

                if (json != null) {
                    sukses=json.getInt("status");


                    if(sukses==1)
                    {
                        JSONArray jsonArray=json.optJSONArray("userData");
                        for(int i=0;i<jsonArray.length(); i++)
                        {
                            JSONObject jsonObject=jsonArray.getJSONObject(i);
                            id_user=Integer.parseInt(jsonObject.optString("id_user").toString());
                            FullName=jsonObject.optString("fullname").toString();
                            challenge_1=Integer.parseInt(jsonObject.optString("challenge_1").toString());
                            challenge_2=Integer.parseInt(jsonObject.optString("challenge_2").toString());
                            challenge_3=Integer.parseInt(jsonObject.optString("challenge_3").toString());
                            challenge_4=Integer.parseInt(jsonObject.optString("challenge_4").toString());
                            challenge_5=Integer.parseInt(jsonObject.optString("challenge_5").toString());
                            total_points=Integer.parseInt(jsonObject.optString("points_total").toString());

                        }
                        cek=true;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String s) {
            if(cek)
            {
                Log.d("BACA",String.valueOf(id_user));
                savePreferences(Email,Password,id_user,FullName,challenge_1,challenge_2,challenge_3,challenge_4,challenge_5,total_points);
                Intent i=new Intent(login.this,MenuUtama.class);

                startActivity(i);
                Toast.makeText(getApplicationContext(),"Login sukses",Toast.LENGTH_SHORT).show();
                finish();
            }
            else
            {
                Snackbar snackbar = Snackbar.make(coordinatorLayout, "Email atau password salah", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
            pDialog.dismiss();
        }
    }

    public void savePreferences(String email,String pass,int id_user,String FullName,int challenge_1,int challenge_2,int challenge_3,int challenge_4,int challenge_5,int points_total)
    {
        SharedPreferences sp=getApplicationContext().getSharedPreferences("MyShared", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor=sp.edit();
        editor.putString("session","login");
        editor.putString("email",email);
        editor.putString("password",pass);
        editor.putString("id_user",String.valueOf(id_user));
        Log.d("BACA2",String.valueOf(id_user));
        editor.putString("fullname",FullName);
        editor.putInt("challenge_1",challenge_1);
        editor.putInt("challenge_2",challenge_2);
        editor.putInt("challenge_3",challenge_3);
        editor.putInt("challenge_4",challenge_4);
        editor.putInt("challenge_5",challenge_5);
        editor.putInt("points_total",points_total);
        editor.apply();
    }
}
