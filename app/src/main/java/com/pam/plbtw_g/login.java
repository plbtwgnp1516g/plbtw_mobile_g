package com.pam.plbtw_g;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.transition.Slide;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class login extends AppCompatActivity {

    ProgressDialog pDialog;
    JSONParser jsonparser;
    EditText email,pass;
    public int sukses;
    private Activity act;
    public boolean cek = false;
    private CoordinatorLayout coordinatorLayout;
    JSONArray names = null;
    private Button btnlogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        btnlogin=(Button)findViewById(R.id.btnLogin) ;
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.login);
        jsonparser=new JSONParser();
        email=(EditText) findViewById(R.id.txtEmail);
        pass=(EditText) findViewById(R.id.txtPass);
        act=this;
    }


    public void Log_in(View v)
    {
        if(email.length()<=0 || pass.length()<=0)
        {
            YoYo.with(Techniques.Shake)
                    .duration(700)
                    .playOn(findViewById(R.id.txtEmail));
            YoYo.with(Techniques.Shake)
                    .duration(700)
                    .playOn(findViewById(R.id.txtPass));
        }
        else
        {
            new LoginAsynctask(email.getText().toString(),pass.getText().toString()).execute();

        }
        /*if(cek)
        {
            Intent i=new Intent(login.this,MenuUtama.class);
            startActivity(i);
            Toast.makeText(getApplicationContext(),"Login sukses",Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(getApplicationContext(),"e-mail atau password salah",Toast.LENGTH_SHORT).show();
        }*/
    }

    public void Register(View v)
    {
        Intent i=new Intent(login.this,registerActivity.class);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class LoginAsynctask extends AsyncTask<String, String, String> {
        String Email,Password;


        public LoginAsynctask(String Email,String Password) {
            this.Email = Email;
            this.Password = Password;
            pDialog = new ProgressDialog(login.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setMessage("Mengecek Pengguna....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("email", Email));
            params.add(new BasicNameValuePair("password", Password));
            try {
                JSONObject json = jsonparser.makeHttpRequest("http://plbtwtraditionalculture.16mb.com/traditional_culture_api/index.php/api/TraditionalCulture/login", "POST", params);

                //Log.d("Semua Nama:", json.toString());

                if (json != null) {
                    sukses=json.getInt("status");

                    Log.d("Semua Nama:", json.toString());

                    if(sukses==1)
                    {
                        cek=true;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String s) {
            if(cek)
            {



                Intent i=new Intent(login.this,MenuUtama.class);

                startActivity(i);
//                Snackbar snackbar = Snackbar.make(coordinatorLayout, "Login Sukses", Snackbar.LENGTH_SHORT);
//                snackbar.show();
                Toast.makeText(getApplicationContext(),"Login sukses",Toast.LENGTH_SHORT).show();
            }
            else
            {
                Snackbar snackbar = Snackbar.make(coordinatorLayout, "Email atau password salah", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
            pDialog.dismiss();
        }
    }
}
