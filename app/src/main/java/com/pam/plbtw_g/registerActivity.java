package com.pam.plbtw_g;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nicolas Juniar on 24/05/2016.
 */
public class registerActivity extends AppCompatActivity {

    ProgressDialog pDialog;
    JSONParser jsonparser;
    EditText email,fullname,pass,pass2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        jsonparser = new JSONParser();

        email=(EditText) findViewById(R.id.txtEmail);
        fullname=(EditText) findViewById(R.id.txtFullname);
        pass=(EditText) findViewById(R.id.txtPass);
        pass2=(EditText) findViewById(R.id.txtPass2);
    }

    public void Sign_Up(View v)
    {
        if(!pass.getText().toString().equals(pass2.getText().toString()))
        {
            Toast.makeText(getApplicationContext(),"password dan konfirmasi password tidak sama",Toast.LENGTH_SHORT).show();
        }
        else
        {
            new RegisterAsynctask(email.getText().toString(),fullname.getText().toString(),pass.getText().toString()).execute();
        }
    }

    class RegisterAsynctask extends AsyncTask<String, String, String> {
        int sukses = 0;
        String Email,FullName,Password;
        boolean cek = true;

        public RegisterAsynctask(String Email, String Fullname, String Password) {
            this.Email = Email;
            this.FullName = Fullname;
            this.Password = Password;

            boolean cek=true;

            pDialog = new ProgressDialog(registerActivity.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setMessage("Mendaftarkan pengguna....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("email", Email));
            params.add(new BasicNameValuePair("password", Password));
            params.add(new BasicNameValuePair("fullname", FullName));
            try {
                JSONObject json = jsonparser.makeHttpRequest("http://plbtwtraditionalculture.16mb.com/traditional_culture_api/index.php/api/TraditionalCulture/signup", "POST", params);

                if (json != null) {
                    sukses = json.getInt("status");
                    if(sukses==1)
                    {
                        cek=true;
                    }
                    else
                    {
                        cek=false;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String s) {

            pDialog.dismiss();
            if(cek)
            {
                Intent i=new Intent(registerActivity.this,login.class);
                startActivity(i);
                finish();
            }
            else
            {
                Toast.makeText(getApplicationContext(),"E-mail sudah ada, gunakan e-mail lain",Toast.LENGTH_SHORT).show();
            }
        }
    }
}
