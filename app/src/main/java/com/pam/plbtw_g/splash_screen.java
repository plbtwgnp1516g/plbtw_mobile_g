package com.pam.plbtw_g;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Created by Nicolas Juniar on 17/05/2016.
 */
public class splash_screen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
    //    this.overridePendingTransition(R.anim.left_to_right,
           //     R.anim.right_to_left);
        Thread logoTimer = new Thread() {
            public void run() {
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    Log.d("Exception", "Exception" + e);
                } finally {
                    startActivity(new Intent(splash_screen.this, login.class));
                }
                finish();
            }
        };
        logoTimer.start();
    }
}
