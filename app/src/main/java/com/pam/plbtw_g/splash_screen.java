package com.pam.plbtw_g;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Created by Nicolas Juniar on 17/05/2016.
 */
public class splash_screen extends AppCompatActivity {
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        sp=getSharedPreferences("MyShared", Activity.MODE_PRIVATE);
        editor=sp.edit();
    //    this.overridePendingTransition(R.anim.left_to_right,
           //     R.anim.right_to_left);
        Thread logoTimer = new Thread() {
            public void run() {
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    Log.d("Exception", "Exception" + e);
                } finally {
                    if(sp.getString("session","").equals("login"))
                    startActivity(new Intent(splash_screen.this, MenuUtama.class));
                    else
                        startActivity(new Intent(splash_screen.this, login.class));
                }
                finish();
            }
        };
        logoTimer.start();
    }
}
